package com.ly.wxstore.rest.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 封装微信支付接口返回数据
 * @author Peter
 *
 */
@XmlRootElement(name = "xml")
public class WeixinPayRespDto {
	
	public static String SUCCESS="SUCCESS";
	public static String FAIL="FAIL";
	
	@XmlElement
	private String return_code;//SUCCESS/FAIL
	@XmlElement
	private String return_msg;
	
	//以下字段在return_code为SUCCESS的时候有返回
	@XmlElement
	private String appid;
	@XmlElement
	private String mch_id;
	@XmlElement
	private String device_info;
	@XmlElement
	private String nonce_str;
	@XmlElement
	private String sign;
	@XmlElement
	private String result_code;//SUCCESS/FAIL
	@XmlElement
	private String err_code;
	@XmlElement
	private String err_code_des;
	
	//以下字段在return_code 和result_code都为SUCCESS的时候有返回
	/**
	 * 调用接口提交的交易类型，取值如下：JSAPI，NATIVE，APP
	 */
	@XmlElement
	private String trade_type;
	
	/**
	 * 微信生成的预支付回话标识，用于后续接口调用中使用，该值有效期为2小时
	 */
	@XmlElement
	private String prepay_id;
	
	/**
	 * trade_type为NATIVE是有返回，可将该参数值生成二维码展示出来进行扫码支付
	 */
	@XmlElement
	private String code_url;
}
