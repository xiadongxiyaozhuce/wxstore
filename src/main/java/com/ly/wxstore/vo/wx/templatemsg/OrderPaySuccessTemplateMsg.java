package com.ly.wxstore.vo.wx.templatemsg;

/**
 * 模板消息【订单支付成功】
 * 
 * @author Peter
 *
 */
public class OrderPaySuccessTemplateMsg {

	private String touser;
	private String template_id;
	private String url;
	private String topcolor;
	private Data data = new Data();

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTopcolor() {
		return topcolor;
	}

	public void setTopcolor(String topcolor) {
		this.topcolor = topcolor;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {
		private DataItem first = new DataItem();// 示例：我们已收到您的货款，开始为您打包商品，请耐心等待: )
		private DataItem orderMoneySum = new DataItem();// 支付金额，示例：30.00元
		private DataItem orderProductName = new DataItem();// 商品名称，示例：明前雨花茶
		private DataItem Remark = new DataItem();// 如有问题直接在微信留言，店小二将第一时间为您服务！: )

		public DataItem getFirst() {
			return first;
		}

		public void setFirst(DataItem first) {
			this.first = first;
		}

		public DataItem getOrderMoneySum() {
			return orderMoneySum;
		}

		public void setOrderMoneySum(DataItem orderMoneySum) {
			this.orderMoneySum = orderMoneySum;
		}

		public DataItem getOrderProductName() {
			return orderProductName;
		}

		public void setOrderProductName(DataItem orderProductName) {
			this.orderProductName = orderProductName;
		}

		public DataItem getRemark() {
			return Remark;
		}

		public void setRemark(DataItem remark) {
			Remark = remark;
		}

	}

	public class DataItem {
		private String value;
		private String color;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

	}

}
