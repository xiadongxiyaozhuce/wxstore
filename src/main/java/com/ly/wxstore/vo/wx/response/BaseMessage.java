package com.ly.wxstore.vo.wx.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.ly.wxstore.vo.xml.AdapterCDATA;
import com.sun.xml.txw2.annotation.XmlCDATA;

@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseMessage {
	
	// 接收方帐号（收到的OpenID）
	@XmlJavaTypeAdapter(AdapterCDATA.class)
	private String ToUserName;

	// 开发者微信号
	@XmlJavaTypeAdapter(AdapterCDATA.class)
	private String FromUserName;

	// 消息创建时间 （整型）
	private long CreateTime;

	// 消息类型（text/music/news）
	@XmlJavaTypeAdapter(AdapterCDATA.class)
	private String MsgType;

	
	public String getToUserName() {
		return ToUserName;
	}

	@XmlCDATA
	public void setToUserName(String toUserName) {
		ToUserName = toUserName;
	}

	
	public String getFromUserName() {
		return FromUserName;
	}

	public void setFromUserName(String fromUserName) {
		FromUserName = fromUserName;
	}

	
	public long getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(long createTime) {
		CreateTime = createTime;
	}

	
	public String getMsgType() {
		return MsgType;
	}

	public void setMsgType(String msgType) {
		MsgType = msgType;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
