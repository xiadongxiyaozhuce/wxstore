package com.ly.wxstore.vo.wx;

public enum MsgType {
	/**
	 * 文本消息
	 */
	text,

	/**
	 * 图片消息
	 */
	image,

	/**
	 * 语音消息
	 */
	voice,

	/**
	 * 视频消息
	 */
	video,

	/**
	 * 小视频消息
	 */
	shortvideo,

	/**
	 * 地理位置消息
	 */
	location,

	/**
	 * 链接消息
	 */
	link,

	/**
	 * 事件消息
	 */
	event,
	
	/**
	 * 图文消息
	 */
	news
}
