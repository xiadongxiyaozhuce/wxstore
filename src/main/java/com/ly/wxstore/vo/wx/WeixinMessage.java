package com.ly.wxstore.vo.wx;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;

@XmlRootElement(name = "xml")
public class WeixinMessage {

	@XmlElement
	private String ToUserName; // 开发者微信号
	
	@XmlElement
	private String FromUserName; // 发送方帐号（一个OpenID）
	@XmlElement
	private Long CreateTime; // 消息创建时间 （整型）
	@XmlElement
	private String MsgType; // 文本消息为text,图片消息image,语音消息voice,视频为video,地理位置location,链接消息link,事件为event
	@XmlElement
	private String Event; //
	@XmlElement
	private String EventKey; // 事件KEY值，qrscene_为前缀，后面为二维码的参数值
	@XmlElement
	private String Content; // 文本消息内容
	@XmlElement
	private String MsgId; // 消息id，64位整型

	@XmlElement
	private String Ticket; //
	@XmlElement
	private String Latitude; //
	@XmlElement
	private String Longitude; //
	@XmlElement
	private String Precision; //
	@XmlElement
	private String MediaId; // 语音消息媒体id，可以调用多媒体文件下载接口拉取该媒体
	@XmlElement
	private String Format; // 语音格式：amr
	@XmlElement
	private String Recognition; // 语音识别结果，UTF8编码

	public String getToUserName() {
		return ToUserName;
	}

	public void setToUserName(String toUserName) {
		ToUserName = toUserName;
	}

	public String getFromUserName() {
		return FromUserName;
	}

	public void setFromUserName(String fromUserName) {
		FromUserName = fromUserName;
	}

	public Long getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(Long createTime) {
		CreateTime = createTime;
	}

	public String getMsgType() {
		return MsgType;
	}

	public void setMsgType(String msgType) {
		MsgType = msgType;
	}

	public String getEvent() {
		return Event;
	}

	public void setEvent(String event) {
		Event = event;
	}

	public String getEventKey() {
		return EventKey;
	}

	public void setEventKey(String eventKey) {
		EventKey = eventKey;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public String getMsgId() {
		return MsgId;
	}

	public void setMsgId(String msgId) {
		MsgId = msgId;
	}

	public String getTicket() {
		return Ticket;
	}

	public void setTicket(String ticket) {
		Ticket = ticket;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getPrecision() {
		return Precision;
	}

	public void setPrecision(String precision) {
		Precision = precision;
	}

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public String getFormat() {
		return Format;
	}

	public void setFormat(String format) {
		Format = format;
	}

	public String getRecognition() {
		return Recognition;
	}

	public void setRecognition(String recognition) {
		Recognition = recognition;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
