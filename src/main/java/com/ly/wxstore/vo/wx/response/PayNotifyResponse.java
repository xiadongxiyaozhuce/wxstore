package com.ly.wxstore.vo.wx.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.ly.wxstore.vo.xml.AdapterCDATA;

@XmlRootElement(name = "xml")
public class PayNotifyResponse {
	
	private String return_code;
	
	private String return_msg;

	@XmlJavaTypeAdapter(AdapterCDATA.class)
	@XmlElement(name="return_code")
	public String getReturn_code() {
		return return_code;
	}

	public void setReturn_code(String return_code) {
		this.return_code = return_code;
	}

	@XmlJavaTypeAdapter(AdapterCDATA.class)
	@XmlElement(name="return_msg")
	public String getReturn_msg() {
		return return_msg;
	}

	public void setReturn_msg(String return_msg) {
		this.return_msg = return_msg;
	}

}
