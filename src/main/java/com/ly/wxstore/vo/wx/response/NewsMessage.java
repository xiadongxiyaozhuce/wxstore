package com.ly.wxstore.vo.wx.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class NewsMessage extends BaseMessage {
	// 图文消息个数，限制为10条以内
	@XmlElement
	private int ArticleCount;
	// 多条图文消息信息，默认第一个item为大图
	@XmlElement(name = "Articles")
	private Articles item = new Articles();

	public int getArticleCount() {
		return ArticleCount;
	}

	public void setArticleCount(int articleCount) {
		ArticleCount = articleCount;
	}

	public Articles getItem() {
		return item;
	}

	public void setItem(Articles item) {
		this.item = item;
	}

}
