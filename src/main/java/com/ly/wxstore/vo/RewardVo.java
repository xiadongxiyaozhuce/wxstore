package com.ly.wxstore.vo;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class RewardVo {
	
	public static final Long SCORE = 10L;
	
	private String radioGroup1;
	private String radioGroup2;
	private String radioGroup3;
	private String radioGroup4;
	private String radioGroup5;
	private Long systemInstanceId;
	private String from;
	private String khid;
	
	
	public String getRadioGroup1() {
		return radioGroup1;
	}
	public void setRadioGroup1(String radioGroup1) {
		this.radioGroup1 = radioGroup1;
	}
	public String getRadioGroup2() {
		return radioGroup2;
	}
	public void setRadioGroup2(String radioGroup2) {
		this.radioGroup2 = radioGroup2;
	}
	public String getRadioGroup3() {
		return radioGroup3;
	}
	public void setRadioGroup3(String radioGroup3) {
		this.radioGroup3 = radioGroup3;
	}
	public String getRadioGroup4() {
		return radioGroup4;
	}
	public void setRadioGroup4(String radioGroup4) {
		this.radioGroup4 = radioGroup4;
	}
	public String getRadioGroup5() {
		return radioGroup5;
	}
	public void setRadioGroup5(String radioGroup5) {
		this.radioGroup5 = radioGroup5;
	}
	public Long getSystemInstanceId() {
		return systemInstanceId;
	}
	public void setSystemInstanceId(Long systemInstanceId) {
		this.systemInstanceId = systemInstanceId;
	}
	
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	
	public String getKhid() {
		return khid;
	}
	public void setKhid(String khid) {
		this.khid = khid;
	}
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
