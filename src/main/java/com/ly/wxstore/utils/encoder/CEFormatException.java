package com.ly.wxstore.utils.encoder;

import java.io.IOException;

public class CEFormatException extends IOException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6943527652462442854L;

	public CEFormatException(String s)
	{
		super(s);
	}
}
