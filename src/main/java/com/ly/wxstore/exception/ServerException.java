/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.wxstore.exception;

import org.springframework.http.HttpStatus;

/**
 * 专用于服务端的异常.RuntimeException可回滚事物
 * 
 * @author peter
 */
public class ServerException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

	public String code;

	public ServerException() {
	}

	public ServerException(HttpStatus status) {
		this.status = status;
	}

	public ServerException(String code, String message) {
		super(message);
		this.code = code;
	}

	public ServerException(HttpStatus status, String message) {
		super(message);
		this.status = status;
	}

	public ServerException(String code, HttpStatus status, String message) {
		super(message);
		this.status = status;
		this.code = code;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
