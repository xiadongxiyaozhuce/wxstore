package com.ly.wxstore.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;

public class MyCacheEventListener implements CacheEventListener {

	private static Logger logger = LoggerFactory.getLogger(MyCacheEventListener.class);

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String str(Element e){
		
		return "{key:"+e.getObjectKey()+",value:"+e.getObjectValue()+"}";
		
	}
	
	
	@Override
	public void notifyElementRemoved(Ehcache cache, Element element) throws CacheException {
		logger.info("notifyElementRemoved -->>"+str(element));

	}

	@Override
	public void notifyElementPut(Ehcache cache, Element element) throws CacheException {
		logger.info("notifyElementPut -->>"+str(element));

	}

	@Override
	public void notifyElementUpdated(Ehcache cache, Element element) throws CacheException {
		logger.info("notifyElementUpdated -->>"+str(element));

	}

	@Override
	public void notifyElementExpired(Ehcache cache, Element element) {
		logger.info("notifyElementExpired -->>"+str(element));

	}

	@Override
	public void notifyElementEvicted(Ehcache cache, Element element) {
		logger.info("notifyElementEvicted -->>"+str(element));

	}

	@Override
	public void notifyRemoveAll(Ehcache cache) {
		logger.info("notifyRemoveAll");

	}

	@Override
	public void dispose() {
		logger.info("dispose");

	}

}
