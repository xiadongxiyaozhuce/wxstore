package com.ly.wxstore.listener;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Status;
import net.sf.ehcache.event.CacheManagerEventListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyCacheManagerEventListener implements CacheManagerEventListener {

	private static Logger logger = LoggerFactory.getLogger(MyCacheManagerEventListener.class);

	private final CacheManager cacheManager;

	public MyCacheManagerEventListener(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public void init() throws CacheException {
		logger.info("init");

	}

	@Override
	public Status getStatus() {
		logger.info("getStatus");
		return null;
	}

	@Override
	public void dispose() throws CacheException {
		logger.info("dispose");

	}

	@Override
	public void notifyCacheAdded(String cacheName) {
		logger.info("notifyCacheAdded-->>cacheName:" + cacheName);

	}

	@Override
	public void notifyCacheRemoved(String cacheName) {
		logger.info("notifyCacheRemoved-->>cacheName:" + cacheName);
	}

}
