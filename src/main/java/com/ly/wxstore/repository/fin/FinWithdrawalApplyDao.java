package com.ly.wxstore.repository.fin;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.fin.FinWithdrawalApply;
import com.ly.wxstore.repository.MyBatisRepository;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface FinWithdrawalApplyDao {
	
	FinWithdrawalApply getById(Long id);
	
	List<FinWithdrawalApply> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<FinWithdrawalApply> searchPage(@Param("finWithdrawalApply")FinWithdrawalApply finWithdrawalApply,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(FinWithdrawalApply finWithdrawalApply);
	
	void save(FinWithdrawalApply finWithdrawalApply);
	
	void update(FinWithdrawalApply finWithdrawalApply);
	
	/**
	 * 软删除
	 */
	void delete(Long id);
	

}
