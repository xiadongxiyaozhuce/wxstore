package com.ly.wxstore.repository.weixin;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.weixin.WeixinH5payNotify;
import com.ly.wxstore.repository.MyBatisRepository;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface WeixinH5payNotifyDao {
	
	WeixinH5payNotify getById(Long id);
	
	List<WeixinH5payNotify> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<WeixinH5payNotify> searchPage(@Param("weixinH5payNotify")WeixinH5payNotify weixinH5payNotify,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(WeixinH5payNotify weixinH5payNotify);
	
	void save(WeixinH5payNotify weixinH5payNotify);
	
	void update(WeixinH5payNotify weixinH5payNotify);
	
	/**
	 * 软删除
	 */
	void delete(Long id);

	List<WeixinH5payNotify> getByOrderCode(String outTradeNo);
	

}
