package com.ly.wxstore.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.SalerUpHierarchy;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface SalerUpHierarchyDao {
	
	SalerUpHierarchy getById(Long id);
	
	List<SalerUpHierarchy> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<SalerUpHierarchy> searchPage(@Param("salerUpHierarchy")SalerUpHierarchy salerUpHierarchy,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(SalerUpHierarchy salerUpHierarchy);
	
	void save(SalerUpHierarchy salerUpHierarchy);
	
	void update(SalerUpHierarchy salerUpHierarchy);
	
	/**
	 * 软删除
	 */
	void delete(Long id);

	SalerUpHierarchy getUpHierarchyBySid(String sid);
	

}
