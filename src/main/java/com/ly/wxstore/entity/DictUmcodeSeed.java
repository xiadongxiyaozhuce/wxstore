package com.ly.wxstore.entity;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class DictUmcodeSeed {

	public DictUmcodeSeed() {
	}
	
	private Long id; //
	private Date createDate; //
	private Long weixinPublicId; //
	private String umcodeSeed; //
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
    /**
     *
     **/
	public String getUmcodeSeed(){
		return umcodeSeed;
	}
	
	/**
	 *
	 **/
	public void setUmcodeSeed(String umcodeSeed){
		this.umcodeSeed=umcodeSeed;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}