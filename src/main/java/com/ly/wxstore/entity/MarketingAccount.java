package com.ly.wxstore.entity;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class MarketingAccount {

	public MarketingAccount() {
	}

	private Long id; //
	private Long pid; //
	private String openid; //
	private String umcode; // 用户营销号编UserMarkingCode(umcode)
	private String sid; //
	private String parentSid; //
	private Long weixinPublicId; //
	private Date createDate; // 创建时间
	private Long boss; // 是否拥有微信公众号（boss）1,是，0.不是
	private String name; // 姓名
	private String phone; // 手机
	private Long depth; // 树的深度
	private String salerRole;// 销售角色：fans（粉丝）、broker(经纪人)、partner(合伙人)
	private String topSid;// 系统预留字段：用于支持合伙人的销售模式
	private String registerFrom;// 注册账号方式：scan：扫码关注，direct：直接关注，oauth：OAuth页面授权注册账号

	/**
     *
     **/
	public Long getId() {
		return id;
	}

	/**
	 *
	 **/
	public void setId(Long id) {
		this.id = id;
	}

	public String getRegisterFrom() {
		return registerFrom;
	}

	public void setRegisterFrom(String registerFrom) {
		this.registerFrom = registerFrom;
	}

	/**
     *
     **/
	public Long getPid() {
		return pid;
	}

	/**
	 *
	 **/
	public void setPid(Long pid) {
		this.pid = pid;
	}

	/**
     *
     **/
	public String getOpenid() {
		return openid;
	}

	/**
	 *
	 **/
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	/**
	 * 用户营销号编UserMarkingCode(umcode)
	 **/
	public String getUmcode() {
		return umcode;
	}

	/**
	 * 用户营销号编UserMarkingCode(umcode)
	 **/
	public void setUmcode(String umcode) {
		this.umcode = umcode;
	}

	/**
     *
     **/
	public String getSid() {
		return sid;
	}

	/**
	 *
	 **/
	public void setSid(String sid) {
		this.sid = sid;
	}

	/**
     *
     **/
	public String getParentSid() {
		return parentSid;
	}

	/**
	 *
	 **/
	public void setParentSid(String parentSid) {
		this.parentSid = parentSid;
	}

	/**
     *
     **/
	public Long getWeixinPublicId() {
		return weixinPublicId;
	}

	/**
	 *
	 **/
	public void setWeixinPublicId(Long weixinPublicId) {
		this.weixinPublicId = weixinPublicId;
	}

	/**
	 * 创建时间
	 **/
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * 创建时间
	 **/
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 是否拥有微信公众号（boss）1,是，0.不是
	 **/
	public Long getBoss() {
		return boss;
	}

	/**
	 * 是否拥有微信公众号（boss）1,是，0.不是
	 **/
	public void setBoss(Long boss) {
		this.boss = boss;
	}

	/**
	 * 姓名
	 **/
	public String getName() {
		return name;
	}

	/**
	 * 姓名
	 **/
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 手机
	 **/
	public String getPhone() {
		return phone;
	}

	/**
	 * 手机
	 **/
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 树的深度
	 **/
	public Long getDepth() {
		return depth;
	}

	/**
	 * 树的深度
	 **/
	public void setDepth(Long depth) {
		this.depth = depth;
	}

	public String getSalerRole() {
		return salerRole;
	}

	public void setSalerRole(String salerRole) {
		this.salerRole = salerRole;
	}

	public String getTopSid() {
		return topSid;
	}

	public void setTopSid(String topSid) {
		this.topSid = topSid;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}