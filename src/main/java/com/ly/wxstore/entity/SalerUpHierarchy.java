package com.ly.wxstore.entity;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class SalerUpHierarchy {

	public SalerUpHierarchy() {
	}
	
	private Long id; //
	private String sid; //
	private String upLevel1Sid; //用户营销号编UserMarkingCode(umcode)
	private String upLevel2Sid; //
	private String upLevel3Sid; //
	private Date createDate; //创建时间
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public String getSid(){
		return sid;
	}
	
	/**
	 *
	 **/
	public void setSid(String sid){
		this.sid=sid;
	}
    /**
     *用户营销号编UserMarkingCode(umcode)
     **/
	public String getUpLevel1Sid(){
		return upLevel1Sid;
	}
	
	/**
	 *用户营销号编UserMarkingCode(umcode)
	 **/
	public void setUpLevel1Sid(String upLevel1Sid){
		this.upLevel1Sid=upLevel1Sid;
	}
    /**
     *
     **/
	public String getUpLevel2Sid(){
		return upLevel2Sid;
	}
	
	/**
	 *
	 **/
	public void setUpLevel2Sid(String upLevel2Sid){
		this.upLevel2Sid=upLevel2Sid;
	}
    /**
     *
     **/
	public String getUpLevel3Sid(){
		return upLevel3Sid;
	}
	
	/**
	 *
	 **/
	public void setUpLevel3Sid(String upLevel3Sid){
		this.upLevel3Sid=upLevel3Sid;
	}
    /**
     *创建时间
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *创建时间
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}