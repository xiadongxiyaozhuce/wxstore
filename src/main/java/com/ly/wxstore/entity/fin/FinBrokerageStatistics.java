package com.ly.wxstore.entity.fin;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class FinBrokerageStatistics {

	public FinBrokerageStatistics() {
	}
	
	private Long id; //
	private String sid; //
	private Double unpayBrokerage; //未付款订单佣金
	private Double payedBrokerage; //已付款订单佣金(待发货、待收货)
	private Double cancelBrokerage; //取消订单佣金
	private Double applayRefundBrokerage; //申请退款佣金
	private Double refundBrokerage; //已退款佣金
	private Double receivedBrokerage; //已收货订单佣金(收货7天内)
	private Double brokerage; //可提现佣金(收货7天后)
	private Double waitAuditBrokerage; //待审核提现佣金
	private Double withdrawBrokerage; //已提现佣金
	private Long weixinPublicId; //微信公众号id
	private Date createDate; //
	private Date updateDate; //
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public String getSid(){
		return sid;
	}
	
	/**
	 *
	 **/
	public void setSid(String sid){
		this.sid=sid;
	}
    /**
     *未付款订单佣金
     **/
	public Double getUnpayBrokerage(){
		return unpayBrokerage;
	}
	
	/**
	 *未付款订单佣金
	 **/
	public void setUnpayBrokerage(Double unpayBrokerage){
		this.unpayBrokerage=unpayBrokerage;
	}
    /**
     *已付款订单佣金(待发货、待收货)
     **/
	public Double getPayedBrokerage(){
		return payedBrokerage;
	}
	
	/**
	 *已付款订单佣金(待发货、待收货)
	 **/
	public void setPayedBrokerage(Double payedBrokerage){
		this.payedBrokerage=payedBrokerage;
	}
    /**
     *取消订单佣金
     **/
	public Double getCancelBrokerage(){
		return cancelBrokerage;
	}
	
	/**
	 *取消订单佣金
	 **/
	public void setCancelBrokerage(Double cancelBrokerage){
		this.cancelBrokerage=cancelBrokerage;
	}
    /**
     *申请退款佣金
     **/
	public Double getApplayRefundBrokerage(){
		return applayRefundBrokerage;
	}
	
	/**
	 *申请退款佣金
	 **/
	public void setApplayRefundBrokerage(Double applayRefundBrokerage){
		this.applayRefundBrokerage=applayRefundBrokerage;
	}
    /**
     *已退款佣金
     **/
	public Double getRefundBrokerage(){
		return refundBrokerage;
	}
	
	/**
	 *已退款佣金
	 **/
	public void setRefundBrokerage(Double refundBrokerage){
		this.refundBrokerage=refundBrokerage;
	}
    /**
     *已收货订单佣金(收货7天内)
     **/
	public Double getReceivedBrokerage(){
		return receivedBrokerage;
	}
	
	/**
	 *已收货订单佣金(收货7天内)
	 **/
	public void setReceivedBrokerage(Double receivedBrokerage){
		this.receivedBrokerage=receivedBrokerage;
	}
    /**
     *可提现佣金(收货7天后)
     **/
	public Double getBrokerage(){
		return brokerage;
	}
	
	/**
	 *可提现佣金(收货7天后)
	 **/
	public void setBrokerage(Double brokerage){
		this.brokerage=brokerage;
	}
    /**
     *待审核提现佣金
     **/
	public Double getWaitAuditBrokerage(){
		return waitAuditBrokerage;
	}
	
	/**
	 *待审核提现佣金
	 **/
	public void setWaitAuditBrokerage(Double waitAuditBrokerage){
		this.waitAuditBrokerage=waitAuditBrokerage;
	}
    /**
     *已提现佣金
     **/
	public Double getWithdrawBrokerage(){
		return withdrawBrokerage;
	}
	
	/**
	 *已提现佣金
	 **/
	public void setWithdrawBrokerage(Double withdrawBrokerage){
		this.withdrawBrokerage=withdrawBrokerage;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}