package com.ly.wxstore.entity.fin;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class FinRefundApply {

	public FinRefundApply() {
	}
	
	private Long id; //
	private String applySid; //申请人sid
	private Date applyDate; //申请退款时间
	private Double money; //金额
	private Long status; //状态（1:待审核,2:审核通过,3:不通过）
	private Date auditDate; //审核时间
	private String auditUid; //审核人uid
	private Long paymentMethodId; //收款方式id
	private Long weixinPublicId; //微信公众号id
	private String orderCode; //订单code
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *申请人sid
     **/
	public String getApplySid(){
		return applySid;
	}
	
	/**
	 *申请人sid
	 **/
	public void setApplySid(String applySid){
		this.applySid=applySid;
	}
    /**
     *申请退款时间
     **/
	public Date getApplyDate(){
		return applyDate;
	}
	
	/**
	 *申请退款时间
	 **/
	public void setApplyDate(Date applyDate){
		this.applyDate=applyDate;
	}
    /**
     *金额
     **/
	public Double getMoney(){
		return money;
	}
	
	/**
	 *金额
	 **/
	public void setMoney(Double money){
		this.money=money;
	}
    /**
     *状态（1:待审核,2:审核通过,3:不通过）
     **/
	public Long getStatus(){
		return status;
	}
	
	/**
	 *状态（1:待审核,2:审核通过,3:不通过）
	 **/
	public void setStatus(Long status){
		this.status=status;
	}
    /**
     *审核时间
     **/
	public Date getAuditDate(){
		return auditDate;
	}
	
	/**
	 *审核时间
	 **/
	public void setAuditDate(Date auditDate){
		this.auditDate=auditDate;
	}
    /**
     *审核人uid
     **/
	public String getAuditUid(){
		return auditUid;
	}
	
	/**
	 *审核人uid
	 **/
	public void setAuditUid(String auditUid){
		this.auditUid=auditUid;
	}
    /**
     *收款方式id
     **/
	public Long getPaymentMethodId(){
		return paymentMethodId;
	}
	
	/**
	 *收款方式id
	 **/
	public void setPaymentMethodId(Long paymentMethodId){
		this.paymentMethodId=paymentMethodId;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
    /**
     *订单code
     **/
	public String getOrderCode(){
		return orderCode;
	}
	
	/**
	 *订单code
	 **/
	public void setOrderCode(String orderCode){
		this.orderCode=orderCode;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}