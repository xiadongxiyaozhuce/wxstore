package com.ly.wxstore.entity.weixin;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class WeixinEventPushArticle {

	public WeixinEventPushArticle() {
	}
	
	private Long id; //
	private Date createDate; //
	private Date updateDate; //
	private String pushEventCode; //事件类型：click菜单点击事件，text图文消息指令事件（比如用户回复“抽奖”，将会推送抽奖页面）
	private String description; //
	private String title; //
	private String picUrl; //
	private String url; //
	private Long seq; //
	private Long weixinPublicId; //微信公众号id
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *事件类型：click菜单点击事件，text图文消息指令事件（比如用户回复“抽奖”，将会推送抽奖页面）
     **/
	public String getPushEventCode(){
		return pushEventCode;
	}
	
	/**
	 *事件类型：click菜单点击事件，text图文消息指令事件（比如用户回复“抽奖”，将会推送抽奖页面）
	 **/
	public void setPushEventCode(String pushEventCode){
		this.pushEventCode=pushEventCode;
	}
    /**
     *
     **/
	public String getDescription(){
		return description;
	}
	
	/**
	 *
	 **/
	public void setDescription(String description){
		this.description=description;
	}
    /**
     *
     **/
	public String getTitle(){
		return title;
	}
	
	/**
	 *
	 **/
	public void setTitle(String title){
		this.title=title;
	}
    /**
     *
     **/
	public String getPicUrl(){
		return picUrl;
	}
	
	/**
	 *
	 **/
	public void setPicUrl(String picUrl){
		this.picUrl=picUrl;
	}
    /**
     *
     **/
	public String getUrl(){
		return url;
	}
	
	/**
	 *
	 **/
	public void setUrl(String url){
		this.url=url;
	}
    /**
     *
     **/
	public Long getSeq(){
		return seq;
	}
	
	/**
	 *
	 **/
	public void setSeq(Long seq){
		this.seq=seq;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}