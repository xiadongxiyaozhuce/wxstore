package com.ly.wxstore.entity.weixin;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class WeixinJsapiTicket {

	public WeixinJsapiTicket() {
	}
	
	private Long id; //
	private String jsapiTicket; //获取到的凭证
	private Long expiresIn; //凭证有效时间，单位：秒
	private Date createDate; //
	private Date updateDate; //
	private Long weixinPublicId; //微信公众号id
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *获取到的凭证
     **/
	public String getJsapiTicket(){
		return jsapiTicket;
	}
	
	/**
	 *获取到的凭证
	 **/
	public void setJsapiTicket(String jsapiTicket){
		this.jsapiTicket=jsapiTicket;
	}
    /**
     *凭证有效时间，单位：秒
     **/
	public Long getExpiresIn(){
		return expiresIn;
	}
	
	/**
	 *凭证有效时间，单位：秒
	 **/
	public void setExpiresIn(Long expiresIn){
		this.expiresIn=expiresIn;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}