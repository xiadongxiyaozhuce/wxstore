package com.ly.wxstore.entity.weixin;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.ly.wxstore.utils.encoder.BASE64Decoder;
import com.ly.wxstore.utils.encoder.BASE64Encoder;

public class WeixinUserInfo {

	public WeixinUserInfo() {
	}

	private Long id; //
	private String openid; // 用户的唯一标识
	private String nickname; // 用户昵称
	private String sex; // 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	private String province; // 用户个人资料填写的省份
	private String city; // 普通用户个人资料填写的城市
	private String country; // 国家，如中国为CN
	private String headimgurl; // 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
	private String privilege; // 用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
	private String unionid; // 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。详见：获取用户个人信息（UnionID机制）
	private Date createDate; //
	private Date updateDate; //
	private Long weixinPublicId; // 微信公众号id

	/**
     *
     **/
	public Long getId() {
		return id;
	}

	/**
	 *
	 **/
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 用户的唯一标识
	 **/
	public String getOpenid() {
		return openid;
	}

	/**
	 * 用户的唯一标识
	 **/
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	/**
	 * 用户昵称
	 **/
	public String getNickname() {
		return nickname;
	}

	/**
	 * 用户昵称
	 **/
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	 **/
	public String getSex() {
		return sex;
	}

	/**
	 * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	 **/
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * 用户个人资料填写的省份
	 **/
	public String getProvince() {
		return province;
	}

	/**
	 * 用户个人资料填写的省份
	 **/
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * 普通用户个人资料填写的城市
	 **/
	public String getCity() {
		return city;
	}

	/**
	 * 普通用户个人资料填写的城市
	 **/
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * 国家，如中国为CN
	 **/
	public String getCountry() {
		return country;
	}

	/**
	 * 国家，如中国为CN
	 **/
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。
	 * 若用户更换头像，原有头像URL将失效。
	 **/
	public String getHeadimgurl() {
		return headimgurl;
	}

	/**
	 * 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。
	 * 若用户更换头像，原有头像URL将失效。
	 **/
	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}

	/**
	 * 用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
	 **/
	public String getPrivilege() {
		return privilege;
	}

	/**
	 * 用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
	 **/
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}

	/**
	 * 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。详见：获取用户个人信息（UnionID机制）
	 **/
	public String getUnionid() {
		return unionid;
	}

	/**
	 * 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。详见：获取用户个人信息（UnionID机制）
	 **/
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	/**
     *
     **/
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 *
	 **/
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
     *
     **/
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 *
	 **/
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getWeixinPublicId() {
		return weixinPublicId;
	}

	public void setWeixinPublicId(Long weixinPublicId) {
		this.weixinPublicId = weixinPublicId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	/**
	 * 获取用户名称，用base64解码
	 * 
	 * @return
	 */
	public String getNicknameForBase64Decoder() {
		BASE64Decoder decoder = new BASE64Decoder();
		String str = "";
		try {
			byte[] nicknameByte = decoder.decodeBuffer(this.nickname == null ? "" : this.nickname);
			str = new String(nicknameByte);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str;
	}

	/**
	 * 用户昵称 用base64编码
	 **/
	public void setNicknameForBase64Encoder(String nickname) {

		BASE64Encoder encoder = new BASE64Encoder();
		this.nickname = encoder.encode(nickname.getBytes());
	}
}