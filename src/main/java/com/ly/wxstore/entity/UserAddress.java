package com.ly.wxstore.entity;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class UserAddress {

	public UserAddress() {
	}

	private Long id; //
	private String sid; // sid
	private String country; // 国家
	private String province; // 省
	private String city; // 市
	private String area; // 区
	private String street; // 街道
	private String detail; // 详细地址
	private String zip; // 邮编
	private Long defaulted; // 是否默认收货地址(1：是，0：否)
	private Long deleted; // 是否默认收货地址(1：是，0：否)
	private String phone; // 收货人手机
	private String name; // 收货人姓名
	private Date createDate; // 
	private Date updateDate; // 
	
	// ++++++++++++++++++++++++++++++++++++++++++++
	private boolean checked = false;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getDeleted() {
		return deleted;
	}

	public void setDeleted(Long deleted) {
		this.deleted = deleted;
	}

	/**
     *
     **/
	public Long getId() {
		return id;
	}

	/**
	 *
	 **/
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * sid
	 **/
	public String getSid() {
		return sid;
	}

	/**
	 * sid
	 **/
	public void setSid(String sid) {
		this.sid = sid;
	}

	/**
	 * 国家
	 **/
	public String getCountry() {
		return country;
	}

	/**
	 * 国家
	 **/
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * 省
	 **/
	public String getProvince() {
		return province;
	}

	/**
	 * 省
	 **/
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * 市
	 **/
	public String getCity() {
		return city;
	}

	/**
	 * 市
	 **/
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * 区
	 **/
	public String getArea() {
		return area;
	}

	/**
	 * 区
	 **/
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * 街道
	 **/
	public String getStreet() {
		return street;
	}

	/**
	 * 街道
	 **/
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * 详细地址
	 **/
	public String getDetail() {
		return detail;
	}

	/**
	 * 详细地址
	 **/
	public void setDetail(String detail) {
		this.detail = detail;
	}

	/**
	 * 邮编
	 **/
	public String getZip() {
		return zip;
	}

	/**
	 * 邮编
	 **/
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * 是否默认收货地址(1：是，0：否)
	 **/
	public Long getDefaulted() {
		return defaulted;
	}

	/**
	 * 是否默认收货地址(1：是，0：否)
	 **/
	public void setDefaulted(Long defaulted) {
		this.defaulted = defaulted;
	}

	/**
	 * 收货人手机
	 **/
	public String getPhone() {
		return phone;
	}

	/**
	 * 收货人手机
	 **/
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 收货人姓名
	 **/
	public String getName() {
		return name;
	}

	/**
	 * 收货人姓名
	 **/
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}