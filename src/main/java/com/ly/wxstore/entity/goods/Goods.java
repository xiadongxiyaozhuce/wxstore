package com.ly.wxstore.entity.goods;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.ly.wxstore.comm.GoodsStatus;

public class Goods {

	public Goods() {
	}

	private Long id; //
	private Date createDate; // 创建时间
	private Date updateDate; // 最后更新时间
	private String createUserUid; // 创建人uid
	private String updateUserUid; // 最后更新人uid
	private Integer deleted; // 是否删除（0：删，1：用）
	private Long weixinPublicId; // 微信公众号id
	private String goodsName; //
	private String goodsDesc; //
	private Double costPrice; // 进货价格
	private Double price; // 销售价格（折前价格）
	private Double realPrice; // 实际销售价格（折后价格）
	private String goodsImageUrl; //
	private Long goodsStatus; //
	private Long inventory; // 库存
	private Long goodsOrder; // 库存

	public Long getGoodsOrder() {
		return goodsOrder;
	}

	public void setGoodsOrder(Long goodsOrder) {
		this.goodsOrder = goodsOrder;
	}

	/**
     *
     **/
	public Long getId() {
		return id;
	}

	/**
	 *
	 **/
	public void setId(Long id) {
		this.id = id;
	}

	public Long getInventory() {
		return inventory;
	}

	public void setInventory(Long inventory) {
		this.inventory = inventory;
	}

	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	/**
	 * 创建时间
	 **/
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * 创建时间
	 **/
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 最后更新时间
	 **/
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * 最后更新时间
	 **/
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * 创建人uid
	 **/
	public String getCreateUserUid() {
		return createUserUid;
	}

	/**
	 * 创建人uid
	 **/
	public void setCreateUserUid(String createUserUid) {
		this.createUserUid = createUserUid;
	}

	/**
	 * 最后更新人uid
	 **/
	public String getUpdateUserUid() {
		return updateUserUid;
	}

	/**
	 * 最后更新人uid
	 **/
	public void setUpdateUserUid(String updateUserUid) {
		this.updateUserUid = updateUserUid;
	}

	/**
	 * 是否删除（0：删，1：用）
	 **/
	public Integer getDeleted() {
		return deleted;
	}

	/**
	 * 是否删除（0：删，1：用）
	 **/
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	/**
	 * 微信公众号id
	 **/
	public Long getWeixinPublicId() {
		return weixinPublicId;
	}

	/**
	 * 微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId) {
		this.weixinPublicId = weixinPublicId;
	}

	/**
     *
     **/
	public String getGoodsName() {
		return goodsName;
	}

	/**
	 *
	 **/
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	/**
     *
     **/
	public String getGoodsDesc() {
		return goodsDesc;
	}

	/**
	 *
	 **/
	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}

	/**
     *
     **/
	public Double getPrice() {
		return price;
	}

	/**
	 *
	 **/
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
     *
     **/
	public Double getRealPrice() {
		return realPrice;
	}

	/**
	 *
	 **/
	public void setRealPrice(Double realPrice) {
		this.realPrice = realPrice;
	}

	public String getGoodsImageUrl() {
		return goodsImageUrl;
	}

	public void setGoodsImageUrl(String goodsImageUrl) {
		this.goodsImageUrl = goodsImageUrl;
	}

	public void setGoodsStatusName(String goodsStatusName) {
		this.goodsStatusName = goodsStatusName;
	}

	/**
     *
     **/
	public Long getGoodsStatus() {
		return goodsStatus;
	}

	/**
	 *
	 **/
	public void setGoodsStatus(Long goodsStatus) {
		this.goodsStatus = goodsStatus;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	private String goodsStatusName; //
	private String publicName; //
	private Double levelOneBrokerage; // 一级佣金
	private Double levelTwoBrokerage; // 二级佣金
	private Double levelThreeBrokerage; // 三级佣金

	public Double getLevelOneBrokerage() {
		return levelOneBrokerage;
	}

	public void setLevelOneBrokerage(Double levelOneBrokerage) {
		this.levelOneBrokerage = levelOneBrokerage;
	}

	public Double getLevelTwoBrokerage() {
		return levelTwoBrokerage;
	}

	public void setLevelTwoBrokerage(Double levelTwoBrokerage) {
		this.levelTwoBrokerage = levelTwoBrokerage;
	}

	public Double getLevelThreeBrokerage() {
		return levelThreeBrokerage;
	}

	public void setLevelThreeBrokerage(Double levelThreeBrokerage) {
		this.levelThreeBrokerage = levelThreeBrokerage;
	}

	public String getGoodsStatusName() {
		goodsStatusName = "";
		if (goodsStatus != null) {
			if (goodsStatus.longValue() == GoodsStatus.Enabled) {
				goodsStatusName = GoodsStatus.Enabled_name;
			} else if (goodsStatus.longValue() == GoodsStatus.Disabled) {
				goodsStatusName = GoodsStatus.Disabled_name;
			}
		}
		return goodsStatusName;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	private List<GoodsImages> images;

	public List<GoodsImages> getImages() {
		return images;
	}

	public void setImages(List<GoodsImages> images) {
		this.images = images;
	}

}