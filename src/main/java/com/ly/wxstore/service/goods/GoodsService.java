package com.ly.wxstore.service.goods;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.goods.Goods;
import com.ly.wxstore.repository.goods.GoodsDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class GoodsService {

	@Autowired
	private GoodsDao goodsDao;

	public Goods getById(Long id) {
		return goodsDao.getById(id);
	}

	public List<Goods> getAll() {
		return goodsDao.getAll();
	}
	
	public List<Goods> getByGoodsName(String name) {
		return goodsDao.getByGoodsName(name);
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<Goods> searchPage(Goods goods, int currentPage, int pageSize) {
		MyPage<Goods> myPage = new MyPage<Goods>();

		Long count = goodsDao.searchCount(goods);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<Goods> list = goodsDao.searchPage(goods, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(Goods goods) {
		goodsDao.save(goods);
	}

	public void update(Goods goods) {
		
		goodsDao.update(goods);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		goodsDao.delete(id);
	}

	public List<Goods> getByWeixinPublicId(Long publicId) {
		return goodsDao.getByWeixinPublicId(publicId);
	}

	
}
