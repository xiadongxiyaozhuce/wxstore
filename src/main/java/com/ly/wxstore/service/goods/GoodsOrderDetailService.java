package com.ly.wxstore.service.goods;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.goods.GoodsOrderDetail;
import com.ly.wxstore.repository.goods.GoodsOrderDetailDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class GoodsOrderDetailService {

	@Autowired
	private GoodsOrderDetailDao goodsOrderDetailDao;

	public GoodsOrderDetail getById(Long id) {
		return goodsOrderDetailDao.getById(id);
	}

	public List<GoodsOrderDetail> getAll() {
		return goodsOrderDetailDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<GoodsOrderDetail> searchPage(GoodsOrderDetail goodsOrderDetail, int currentPage, int pageSize) {
		MyPage<GoodsOrderDetail> myPage = new MyPage<GoodsOrderDetail>();

		Long count = goodsOrderDetailDao.searchCount(goodsOrderDetail);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<GoodsOrderDetail> list = goodsOrderDetailDao.searchPage(goodsOrderDetail, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(GoodsOrderDetail goodsOrderDetail) {
		goodsOrderDetailDao.save(goodsOrderDetail);
	}

	public void update(GoodsOrderDetail goodsOrderDetail) {
		goodsOrderDetailDao.update(goodsOrderDetail);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		goodsOrderDetailDao.delete(id);
	}

	public List<GoodsOrderDetail> getByGoodsOrderCode(String orderCode) {
		// TODO Auto-generated method stub
		return goodsOrderDetailDao.getByGoodsOrderCode(orderCode);
	}
}
