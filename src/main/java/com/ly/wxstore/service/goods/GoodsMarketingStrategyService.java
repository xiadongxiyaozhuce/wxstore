package com.ly.wxstore.service.goods;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.goods.GoodsMarketingStrategy;
import com.ly.wxstore.repository.goods.GoodsMarketingStrategyDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class GoodsMarketingStrategyService {

	@Autowired
	private GoodsMarketingStrategyDao goodsMarketingStrategyDao;

	public GoodsMarketingStrategy getByGoodsId(Long goodsId) {
		return goodsMarketingStrategyDao.getByGoodsId(goodsId);
	}

	public List<GoodsMarketingStrategy> getAll() {
		return goodsMarketingStrategyDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<GoodsMarketingStrategy> searchPage(GoodsMarketingStrategy goodsMarketingStrategy, int currentPage, int pageSize) {
		MyPage<GoodsMarketingStrategy> myPage = new MyPage<GoodsMarketingStrategy>();

		Long count = goodsMarketingStrategyDao.searchCount(goodsMarketingStrategy);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<GoodsMarketingStrategy> list = goodsMarketingStrategyDao.searchPage(goodsMarketingStrategy, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(GoodsMarketingStrategy goodsMarketingStrategy) {
		goodsMarketingStrategyDao.save(goodsMarketingStrategy);
	}

	public void update(GoodsMarketingStrategy goodsMarketingStrategy) {
		goodsMarketingStrategyDao.update(goodsMarketingStrategy);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		goodsMarketingStrategyDao.delete(id);
	}
	
}
