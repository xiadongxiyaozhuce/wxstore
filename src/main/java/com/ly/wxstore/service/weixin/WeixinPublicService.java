package com.ly.wxstore.service.weixin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.weixin.WeixinPublic;
import com.ly.wxstore.repository.weixin.WeixinPublicDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinPublicService {

	@Autowired
	private WeixinConf weixinConf;
	
	private static WeixinPublic weixinPublic;
	
	@Autowired
	private WeixinPublicDao weixinPublicDao;
	
	/**
	 * 获取微信公众号配置信息
	 * @return
	 */
	public WeixinPublic getGlobleWeixinPublic(){
		if(weixinPublic==null){
			weixinPublic = weixinPublicDao.getById(weixinConf.getPublicId());
		}
		
		return weixinPublic;
	}

	public WeixinPublic getById(Long id) {
		return weixinPublicDao.getById(id);
	}

	public List<WeixinPublic> getAll() {
		return weixinPublicDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinPublic> searchPage(WeixinPublic weixinPublic, int currentPage, int pageSize) {
		MyPage<WeixinPublic> myPage = new MyPage<WeixinPublic>();

		Long count = weixinPublicDao.searchCount(weixinPublic);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinPublic> list = weixinPublicDao.searchPage(weixinPublic, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinPublic weixinPublic) {
		weixinPublicDao.save(weixinPublic);
	}

	public void update(WeixinPublic weixinPublic) {
		weixinPublicDao.update(weixinPublic);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinPublicDao.delete(id);
	}

}
