package com.ly.wxstore.service.weixin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.weixin.WeixinTwoDimensionalCode;
import com.ly.wxstore.repository.weixin.WeixinTwoDimensionalCodeDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinTwoDimensionalCodeService {

	@Autowired
	private WeixinTwoDimensionalCodeDao weixinTwoDimensionalCodeDao;

	public WeixinTwoDimensionalCode getById(Long id) {
		return weixinTwoDimensionalCodeDao.getById(id);
	}

	public List<WeixinTwoDimensionalCode> getAll() {
		return weixinTwoDimensionalCodeDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinTwoDimensionalCode> searchPage(WeixinTwoDimensionalCode weixinTwoDimensionalCode, int currentPage, int pageSize) {
		MyPage<WeixinTwoDimensionalCode> myPage = new MyPage<WeixinTwoDimensionalCode>();

		Long count = weixinTwoDimensionalCodeDao.searchCount(weixinTwoDimensionalCode);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinTwoDimensionalCode> list = weixinTwoDimensionalCodeDao.searchPage(weixinTwoDimensionalCode, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinTwoDimensionalCode weixinTwoDimensionalCode) {
		weixinTwoDimensionalCodeDao.save(weixinTwoDimensionalCode);
	}

	public void update(WeixinTwoDimensionalCode weixinTwoDimensionalCode) {
		weixinTwoDimensionalCodeDao.update(weixinTwoDimensionalCode);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinTwoDimensionalCodeDao.delete(id);
	}
}
