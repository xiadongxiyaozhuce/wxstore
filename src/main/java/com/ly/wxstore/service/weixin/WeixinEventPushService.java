package com.ly.wxstore.service.weixin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.weixin.WeixinEventPush;
import com.ly.wxstore.repository.weixin.WeixinEventPushDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinEventPushService {

	@Autowired
	private WeixinEventPushDao weixinEventPushDao;

	public WeixinEventPush getById(Long id) {
		return weixinEventPushDao.getById(id);
	}

	public List<WeixinEventPush> getAll() {
		return weixinEventPushDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinEventPush> searchPage(WeixinEventPush weixinEventPush, int currentPage, int pageSize) {
		MyPage<WeixinEventPush> myPage = new MyPage<WeixinEventPush>();

		Long count = weixinEventPushDao.searchCount(weixinEventPush);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinEventPush> list = weixinEventPushDao.searchPage(weixinEventPush, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinEventPush weixinEventPush) {
		weixinEventPushDao.save(weixinEventPush);
	}

	public void update(WeixinEventPush weixinEventPush) {
		weixinEventPushDao.update(weixinEventPush);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinEventPushDao.delete(id);
	}
}
