package com.ly.wxstore;

import com.ly.wxstore.entity.weixin.WeixinH5payNotify;
import com.ly.wxstore.utils.XmlMapper;

public class XmlTest {
	public static void main(String[] args) {
		String xml="<xml><appid><![CDATA[wxb02a7add18dd8b2b]]></appid>"
					+"<bank_type><![CDATA[CFT]]></bank_type>"
					+"<cash_fee><![CDATA[1]]></cash_fee>"
					+"<device_info><![CDATA[WEB]]></device_info>"
					+"<fee_type><![CDATA[CNY]]></fee_type>"
					+"<is_subscribe><![CDATA[Y]]></is_subscribe>"
					+"<mch_id><![CDATA[1248394601]]></mch_id>"
					+"<nonce_str><![CDATA[EB51D96C8D10408EBB061E6B9E0C8EFB]]></nonce_str>"
					+"<openid><![CDATA[oEnm8vkO-A5Ohb8nI7U-048_T-JM]]></openid>"
					+"<out_trade_no><![CDATA[220150831063854335-24691715]]></out_trade_no>"
					+"<result_code><![CDATA[SUCCESS]]></result_code>"
					+"<return_code><![CDATA[SUCCESS]]></return_code>"
					+"<sign><![CDATA[ECA2F19119260291178A2E57D14DBA16]]></sign>"
					+"<time_end><![CDATA[20150831184232]]></time_end>"
					+"<total_fee>1</total_fee>"
					+"<trade_type><![CDATA[JSAPI]]></trade_type>"
					+"<transaction_id><![CDATA[1003570408201508310755862775]]></transaction_id>"
				+"  </xml>";
		
		
		WeixinH5payNotify  obj = XmlMapper.fromXml(xml, WeixinH5payNotify.class);
		System.out.println(obj.toString());
		
	}

}
